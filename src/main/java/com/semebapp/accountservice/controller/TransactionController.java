package com.semebapp.accountservice.controller;

import com.semebapp.accountservice.entities.Transaction;
import com.semebapp.accountservice.services.TransactionGrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "transaction")
public class TransactionController {

    @Autowired
    private TransactionGrpcService transactionService;

    @GetMapping("/{email}")
    public @ResponseBody
    List<Transaction> getAccounts(@PathVariable String email) {
        return transactionService.getTransactions(email);
    }

    @PostMapping
    public @ResponseBody
    Transaction createTransaction(@RequestBody Transaction transaction) {
        return transactionService.insertTransaction(transaction);
    }

}
