package com.semebapp.accountservice.controller;


import com.semebapp.accountservice.entities.Account;
import com.semebapp.accountservice.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "account")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping
    public @ResponseBody
    List<Account> getAccounts() {
        return accountService.getAccounts();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Account getAccount(@PathVariable String id) {
        return accountService.getAccount(id);
    }

    @PostMapping
    public @ResponseBody
    Account createAccount(@RequestBody Account account) {
        return accountService.createAccount(account);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody
    Account deleteAccount(@PathVariable String id) {
        return this.accountService.deleteAccount(id);
    }

    @PutMapping("/{id}")
    public @ResponseBody
    Account updateAccount(@RequestBody Account account, @PathVariable String id) {
        return this.accountService.updateAccount(account, id);
    }

    @GetMapping("/health")
    public @ResponseBody
    ResponseEntity<String> health() {
        return ResponseEntity.ok("OK");
    }
}
