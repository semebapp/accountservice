package com.semebapp.accountservice.entities;

import javax.persistence.*;

//@Table(name = "preferences")
//@Entity
public class Preferences {
 //   @Id
 //   @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String user;

    private String currencyCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
