package com.semebapp.accountservice.services;

import com.semebapp.accountservice.entities.Account;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AccountService {

    public List<Account> getAccounts();

    Account getAccount(String id);

    Account createAccount(Account account);

    Account updateAccount(Account account, String id);

    Account deleteAccount(String id);
}
