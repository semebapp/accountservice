package com.semebapp.accountservice.services;

import com.google.rpc.Status;
import com.semebapp.accountservice.entities.Transaction;
import com.semebapp.grpc.transactions.*;
import io.grpc.protobuf.StatusProto;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
public class TransactionGrpcService {

    @GrpcClient("transactionservice")
    private TransactionServiceGrpc.TransactionServiceBlockingStub transactionStub;

    public Transaction insertTransaction(Transaction transaction) {
        var request = createFromTransaction(transaction);
        try {
            TransactionResponse response = transactionStub.transact(request);
            return transactionFromGrpc(response);
        } catch (Exception e) {
            Status status = StatusProto.fromThrowable(e);
            System.out.println(status.toString());
            e.printStackTrace();
            return null;
        }
    }

    public List<Transaction> getTransactions(String accountEmail) {
        System.out.println("Getting Transactions for" +accountEmail);
        TransactionRecieveRequest request = TransactionRecieveRequest.newBuilder()
                .setFromAcc(accountEmail)
                .build();
        try {
            TransactionList response = transactionStub.getTransactions(request);
            var transactions = new LinkedList<Transaction>();
            for (TransactionResponse transactionResponse : response.getTransactionList()) {
                transactions.add(transactionFromGrpc(transactionResponse));
            }
            return transactions;
        } catch (Exception e) {
            Status status = StatusProto.fromThrowable(e);
            System.out.println(status.toString());
            e.printStackTrace();
            return null;
        }
    }

    private Transaction transactionFromGrpc(TransactionResponse response) {
        var transaction = new Transaction();
        transaction.setTransactionDate(LocalDateTime.now());
        transaction.setDescription(response.getDescription());
        transaction.setFrom(response.getFromAcc());
        transaction.setTo(response.getToAcc());
        transaction.setFromAmount(response.getAmountFrom());
        transaction.setToAmount(response.getAmountTo());
        return transaction;
    }

    private TransactionCreateRequest createFromTransaction(Transaction transaction) {
        TransactionCreateRequest.Builder builder = TransactionCreateRequest.newBuilder();
        builder.setDescription(transaction.getDescription());
        builder.setAmount(transaction.getFromAmount());
        builder.setFromAcc(transaction.getFrom());
        builder.setToAcc(transaction.getTo());
        return builder.build();
    }

}
