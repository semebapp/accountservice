package com.semebapp.accountservice.services;

import com.semebapp.accountservice.client.PreferencesClient;
import com.semebapp.accountservice.dao.AccountRepository;
import com.semebapp.accountservice.entities.Account;
import com.semebapp.accountservice.entities.Preferences;
import com.semebapp.accountservice.entities.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository repository;

    @Autowired
    TransactionGrpcService grpcService;

    @Autowired
    PreferencesClient preferencesClient;

    @Override
    public List<Account> getAccounts() {
        return repository.findAll();
    }

    @Override
    public Account getAccount(String id) {
        Optional<Account> account = repository.findById(id);
        if (account.isPresent()) {
            var present = account.get();
            var transactions = grpcService.getTransactions(id);
            for (Transaction transaction : transactions) {
                if (transaction.getFrom() == id) {
                    present.setAmount(present.getAmount() - transaction.getFromAmount());
                } else {
                    present.setAmount(present.getAmount() + transaction.getToAmount());
                }
            }
            return present;
        }
        return null;
    }

    @Override
    public Account createAccount(Account account) {
        Account savedAccount = repository.save(account);
        Preferences preferences = new Preferences();
        preferences.setUser(savedAccount.getEmail());
        preferences.setCurrencyCode("EUR");
        Preferences savedPreferences = preferencesClient.setPreferences(preferences);
        if (savedPreferences != null) {
            return savedAccount;
        }
        return null;
    }

    @Override
    public Account updateAccount(Account account, String id) {
        return repository.save(account);
    }

    @Override
    public Account deleteAccount(String id) {
        repository.deleteById(id);
        return null;
    }
}
